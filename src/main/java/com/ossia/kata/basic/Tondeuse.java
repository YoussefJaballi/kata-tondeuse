package com.ossia.kata.basic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Tondeuse {
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("input.txt"));
            String line;
            String[] gridSize = br.readLine().split(" ");
            int maxX = Integer.parseInt(gridSize[0]);
            int maxY = Integer.parseInt(gridSize[1]);

            StringBuilder result = new StringBuilder();

            while ((line = br.readLine()) != null) {
                String[] initialPosition = line.split(" ");
                int startX = Integer.parseInt(initialPosition[0]);
                int startY = Integer.parseInt(initialPosition[1]);
                char startOrientation = initialPosition[2].charAt(0);
                String instructions = br.readLine();

                String finalPosition = processTondeuse(maxX, maxY, startX, startY, startOrientation, instructions);
                result.append(finalPosition).append(" ");
            }

            br.close();

            result.append("NB");
            System.out.println(result.toString().trim());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String processTondeuse(int maxX, int maxY, int startX, int startY, char startOrientation, String instructions) {
        int x = startX;
        int y = startY;
        char orientation = startOrientation;

        for (char instruction : instructions.toCharArray()) {
            if (instruction == 'D') {
                orientation = turnRight(orientation);
            } else if (instruction == 'G') {
                orientation = turnLeft(orientation);
            } else if (instruction == 'A') {
                int newX = x;
                int newY = y;

                if (orientation == 'N') {
                    newY++;
                } else if (orientation == 'E') {
                    newX++;
                } else if (orientation == 'S') {
                    newY--;
                } else if (orientation == 'W') {
                    newX--;
                }

                if (newX >= 0 && newX <= maxX && newY >= 0 && newY <= maxY) {
                    x = newX;
                    y = newY;
                }
            }
        }

        return x + " " + y + " " + orientation;
    }

    public static char turnRight(char orientation) {
        switch (orientation) {
            case 'N':
                return 'E';
            case 'E':
                return 'S';
            case 'S':
                return 'W';
            case 'W':
                return 'N';
            default:
                return orientation;
        }
    }

    public static char turnLeft(char orientation) {
        switch (orientation) {
            case 'N':
                return 'W';
            case 'E':
                return 'N';
            case 'S':
                return 'E';
            case 'W':
                return 'S';
            default:
                return orientation;
        }
    }
}


